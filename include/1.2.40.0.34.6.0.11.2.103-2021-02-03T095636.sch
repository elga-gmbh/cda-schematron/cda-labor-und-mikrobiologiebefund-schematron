<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.103
Name: Befundbewertung
Description: Bemerkungen oder Kommentare, die für den gesamten Befund von Bedeutung sind, werden in der Section "Befundbewertung" am Befundende angeführt.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636">
   <title>Befundbewertung</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]
Item: (atcdabbr_section_Befundbewertung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]
Item: (atcdabbr_section_Befundbewertung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]"
         id="d42e15031-false-d766867e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(atcdabbr_section_Befundbewertung): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atcdabbr_section_Befundbewertung): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']) &gt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']) &lt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:code[(@code = '20' and @codeSystem = '1.2.40.0.34.5.11')]) &gt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:code[(@code = '20' and @codeSystem = '1.2.40.0.34.5.11')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:code[(@code = '20' and @codeSystem = '1.2.40.0.34.5.11')]) &lt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:code[(@code = '20' and @codeSystem = '1.2.40.0.34.5.11')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]) &gt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="count(hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]) &lt;= 1">(atcdabbr_section_Befundbewertung): Element hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']
Item: (atcdabbr_section_Befundbewertung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']"
         id="d42e15040-false-d767084e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_section_Befundbewertung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.103')">(atcdabbr_section_Befundbewertung): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.103' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:code[(@code = '20' and @codeSystem = '1.2.40.0.34.5.11')]
Item: (atcdabbr_section_Befundbewertung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:code[(@code = '20' and @codeSystem = '1.2.40.0.34.5.11')]"
         id="d42e15048-false-d767099e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_section_Befundbewertung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="@nullFlavor or (@code='20' and @codeSystem='1.2.40.0.34.5.11' and @displayName='Befundbewertung')">(atcdabbr_section_Befundbewertung): Der Elementinhalt MUSS einer von 'code '20' codeSystem '1.2.40.0.34.5.11' displayName='Befundbewertung'' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="string(@codeSystemName) = ('ELGA_LaborparameterErgaenzung') or not(@codeSystemName)">(atcdabbr_section_Befundbewertung): Der Wert von codeSystemName MUSS 'ELGA_LaborparameterErgaenzung' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_section_Befundbewertung): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:title[not(@nullFlavor)]
Item: (atcdabbr_section_Befundbewertung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:title[not(@nullFlavor)]"
         id="d42e15056-false-d767121e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_section_Befundbewertung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="text()='Befundbewertung'">(atcdabbr_section_Befundbewertung): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Befundbewertung'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:text[not(@nullFlavor)]
Item: (atcdabbr_section_Befundbewertung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:text[not(@nullFlavor)]"
         id="d42e15062-false-d767135e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(atcdabbr_section_Befundbewertung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:entry[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]
Item: (atcdabbr_section_Befundbewertung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.103
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]
Item: (atcdabbr_section_Befundbewertung)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.103']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_section_Befundbewertung): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_section_Befundbewertung): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
