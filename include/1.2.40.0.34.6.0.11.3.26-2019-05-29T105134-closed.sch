<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.26
Name: Laboratory Battery Organizer
Description: Der "Laboratory Battery Organizer" kann eine  Befundgruppe  innerhalb eines Befundbereiches ("Laboratory Specialty Section") darstellen. Für den "organizer/code" MUSS in diesem Fall ein Code aus dem Level 2 des hierarchischen Value Sets "ELGA_Laborstruktur" verwendet werden. ein  Antibiogramm  innerhalb des "Laboratory Isolate Organizer"
                        darstellen. In diesem Fall MUSS für den "organizer/code" der Code "365705006 - Finding of antimicrobial susceptibility (finding)" angegeben werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134-closed">
   <title>Laboratory Battery Organizer</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/*[not(@xsi:nil = 'true')][not(self::hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']])]"
         id="d42e26592-true-d1599565e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d42e26592-true-d1599565e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']] (rule-reference: d42e26592-true-d1599565e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:code[(@code = '365705006' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | self::hl7:statusCode[@code = 'completed' or @code = 'aborted'] | self::hl7:effectiveTime | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']] | self::hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]] | self::hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]] | self::hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]])]"
         id="d42e26868-true-d1600241e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d42e26868-true-d1600241e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.47-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:code[(@code = '365705006' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | hl7:statusCode[@code = 'completed' or @code = 'aborted'] | hl7:effectiveTime | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']] | hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]] | hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]] | hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] (rule-reference: d42e26868-true-d1600241e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[not(@nullFlavor)] | self::hl7:high[not(@nullFlavor)])]"
         id="d42e26946-true-d1600292e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d42e26946-true-d1600292e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[not(@nullFlavor)] | hl7:high[not(@nullFlavor)] (rule-reference: d42e26946-true-d1600292e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7'] | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d42e26957-true-d1600464e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d42e26957-true-d1600464e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7'] | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d42e26957-true-d1600464e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:code[@nullFlavor='UNK'] | self::hl7:addr[not(@nullFlavor)] | self::hl7:addr[@nullFlavor='UNK'] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:telecom[@nullFlavor='UNK'] | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:assignedPerson[@nullFlavor='UNK'] | self::hl7:representedOrganization)]"
         id="d1600307e65-true-d1600577e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600307e65-true-d1600577e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:code[@nullFlavor='UNK'] | hl7:addr[not(@nullFlavor)] | hl7:addr[@nullFlavor='UNK'] | hl7:telecom[not(@nullFlavor)] | hl7:telecom[@nullFlavor='UNK'] | hl7:assignedPerson | hl7:assignedPerson | hl7:assignedPerson[@nullFlavor='UNK'] | hl7:representedOrganization (rule-reference: d1600307e65-true-d1600577e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1600487e56-true-d1600642e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600487e56-true-d1600642e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1600487e56-true-d1600642e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1600487e104-true-d1600708e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600487e104-true-d1600708e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1600487e104-true-d1600708e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1600487e115-true-d1600730e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600487e115-true-d1600730e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1600487e115-true-d1600730e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1600718e12-true-d1600759e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600718e12-true-d1600759e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1600718e12-true-d1600759e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d1600487e131-true-d1600847e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600487e131-true-d1600847e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d1600487e131-true-d1600847e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1600787e70-true-d1600914e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600787e70-true-d1600914e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1600787e70-true-d1600914e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']])]"
         id="d42e27000-true-d1601441e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d42e27000-true-d1601441e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']] (rule-reference: d42e27000-true-d1601441e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6'] | self::hl7:id | self::hl7:code[not(@nullFlavor)] | self::hl7:code[@nullFlavor='OTH'] | self::hl7:text | self::hl7:statusCode[@code = 'completed' or @code = 'aborted'] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:effectiveTime[@nullFlavor='UNK'] | self::hl7:value[@xsi:type='PQ'] | self::hl7:value[@xsi:type='IVL_PQ'] | self::hl7:value[@xsi:type='INT'] | self::hl7:value[@xsi:type='IVL_INT'] | self::hl7:value[@xsi:type='BL'] | self::hl7:value[@xsi:type='ST'] | self::hl7:value[@xsi:type='CV'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.186-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:value[(@code = '281268007' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | self::hl7:value[(@code = '255599008' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | self::hl7:value[@xsi:type='CD'] | self::hl7:value[@xsi:type='RTO'] | self::hl7:value[@xsi:type='RTO_PQ_PQ'] | self::hl7:interpretationCode[not(@nullFlavor)] | self::hl7:interpretationCode[@nullFlavor='OTH'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']] | self::hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']] | self::hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] | self::hl7:entryRelationship[@typeCode = 'REFR'][hl7:observation[@classCode = 'OBS'][@moodCode = 'EVN']] | self::hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']])]"
         id="d1600959e22-true-d1601986e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e22-true-d1601986e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6'] | hl7:id | hl7:code[not(@nullFlavor)] | hl7:code[@nullFlavor='OTH'] | hl7:text | hl7:statusCode[@code = 'completed' or @code = 'aborted'] | hl7:effectiveTime[not(@nullFlavor)] | hl7:effectiveTime[@nullFlavor='UNK'] | hl7:value[@xsi:type='PQ'] | hl7:value[@xsi:type='IVL_PQ'] | hl7:value[@xsi:type='INT'] | hl7:value[@xsi:type='IVL_INT'] | hl7:value[@xsi:type='BL'] | hl7:value[@xsi:type='ST'] | hl7:value[@xsi:type='CV'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.186-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:value[(@code = '281268007' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | hl7:value[(@code = '255599008' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | hl7:value[@xsi:type='CD'] | hl7:value[@xsi:type='RTO'] | hl7:value[@xsi:type='RTO_PQ_PQ'] | hl7:interpretationCode[not(@nullFlavor)] | hl7:interpretationCode[@nullFlavor='OTH'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']] | hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']] | hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]] | hl7:entryRelationship[@typeCode = 'REFR'][hl7:observation[@classCode = 'OBS'][@moodCode = 'EVN']] | hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']] (rule-reference: d1600959e22-true-d1601986e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1602013e54-true-d1602025e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1602013e54-true-d1602025e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1602013e54-true-d1602025e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:value[@xsi:type='PQ']/*[not(@xsi:nil = 'true')][not(self::hl7:translation)]"
         id="d1602046e23-true-d1602082e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.23-2021-01-19T145323.html"
              test="not(.)">(atcdabbr_other_LaboratoryObservationValue)/d1602046e23-true-d1602082e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation (rule-reference: d1602046e23-true-d1602082e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7'] | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d1600959e455-true-d1602302e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e455-true-d1602302e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7'] | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d1600959e455-true-d1602302e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:code[@nullFlavor='UNK'] | self::hl7:addr[not(@nullFlavor)] | self::hl7:addr[@nullFlavor='UNK'] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:telecom[@nullFlavor='UNK'] | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:assignedPerson[@nullFlavor='UNK'] | self::hl7:representedOrganization)]"
         id="d1602145e67-true-d1602415e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1602145e67-true-d1602415e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:code[@nullFlavor='UNK'] | hl7:addr[not(@nullFlavor)] | hl7:addr[@nullFlavor='UNK'] | hl7:telecom[not(@nullFlavor)] | hl7:telecom[@nullFlavor='UNK'] | hl7:assignedPerson | hl7:assignedPerson | hl7:assignedPerson[@nullFlavor='UNK'] | hl7:representedOrganization (rule-reference: d1602145e67-true-d1602415e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1602325e56-true-d1602480e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1602325e56-true-d1602480e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1602325e56-true-d1602480e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1602325e104-true-d1602546e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1602325e104-true-d1602546e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1602325e104-true-d1602546e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1602325e115-true-d1602568e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1602325e115-true-d1602568e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1602325e115-true-d1602568e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1602556e12-true-d1602597e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1602556e12-true-d1602597e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1602556e12-true-d1602597e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom | self::hl7:addr)]"
         id="d1602325e131-true-d1602685e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1602325e131-true-d1602685e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:name[not(@nullFlavor)] | hl7:telecom | hl7:addr (rule-reference: d1602325e131-true-d1602685e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.7']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1602625e70-true-d1602752e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1602625e70-true-d1602752e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1602625e70-true-d1602752e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5'] | self::hl7:time[not(@nullFlavor)] | self::hl7:participantRole[not(@nullFlavor)][hl7:playingEntity])]"
         id="d1600959e499-true-d1602852e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e499-true-d1602852e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5'] | hl7:time[not(@nullFlavor)] | hl7:participantRole[not(@nullFlavor)][hl7:playingEntity] (rule-reference: d1600959e499-true-d1602852e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']]/hl7:participantRole[not(@nullFlavor)][hl7:playingEntity]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:addr[@nullFlavor='UNK'] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:telecom[@nullFlavor='UNK'] | self::hl7:playingEntity[not(@nullFlavor)])]"
         id="d1600959e546-true-d1602893e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e546-true-d1602893e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:addr[@nullFlavor='UNK'] | hl7:telecom[not(@nullFlavor)] | hl7:telecom[@nullFlavor='UNK'] | hl7:playingEntity[not(@nullFlavor)] (rule-reference: d1600959e546-true-d1602893e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']]/hl7:participantRole[not(@nullFlavor)][hl7:playingEntity]/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1600959e554-true-d1602948e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e554-true-d1602948e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1600959e554-true-d1602948e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:participant[hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.3.1.5']]/hl7:participantRole[not(@nullFlavor)][hl7:playingEntity]/hl7:playingEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1600959e601-true-d1603011e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e601-true-d1603011e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1600959e601-true-d1603011e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']])]"
         id="d1600959e616-true-d1603492e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e616-true-d1603492e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']] (rule-reference: d1600959e616-true-d1603492e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole])]"
         id="d1603021e153-true-d1604012e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1603021e153-true-d1604012e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] (rule-reference: d1603021e153-true-d1604012e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1604043e54-true-d1604055e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1604043e54-true-d1604055e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1604043e54-true-d1604055e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d1603021e223-true-d1604210e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1603021e223-true-d1604210e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d1603021e223-true-d1604210e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1604071e14-true-d1604316e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604071e14-true-d1604316e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1604071e14-true-d1604316e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1604320e101-true-d1604389e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604320e101-true-d1604389e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1604320e101-true-d1604389e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1604320e174-true-d1604465e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1604320e174-true-d1604465e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1604320e174-true-d1604465e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1604320e186-true-d1604487e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1604320e186-true-d1604487e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1604320e186-true-d1604487e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1604475e12-true-d1604516e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1604475e12-true-d1604516e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1604475e12-true-d1604516e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1604320e198-true-d1604567e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604320e198-true-d1604567e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1604320e198-true-d1604567e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1604541e58-true-d1604628e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604541e58-true-d1604628e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1604541e58-true-d1604628e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1603021e226-true-d1604795e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1603021e226-true-d1604795e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1603021e226-true-d1604795e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1604673e45-true-d1604903e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604673e45-true-d1604903e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1604673e45-true-d1604903e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1604673e64-true-d1604962e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604673e64-true-d1604962e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1604673e64-true-d1604962e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1604673e110-true-d1605019e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604673e110-true-d1605019e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1604673e110-true-d1605019e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1605023e92-true-d1605053e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1605023e92-true-d1605053e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1605023e92-true-d1605053e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1604673e133-true-d1605101e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604673e133-true-d1605101e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1604673e133-true-d1605101e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1604673e149-true-d1605146e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1604673e149-true-d1605146e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1604673e149-true-d1605146e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1605116e67-true-d1605209e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605116e67-true-d1605209e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1605116e67-true-d1605209e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1603021e232-true-d1605377e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1603021e232-true-d1605377e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1603021e232-true-d1605377e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1605254e9-true-d1605509e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605254e9-true-d1605509e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1605254e9-true-d1605509e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1605382e50-true-d1605575e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605382e50-true-d1605575e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1605382e50-true-d1605575e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1605382e120-true-d1605637e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605382e120-true-d1605637e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1605382e120-true-d1605637e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1605382e132-true-d1605659e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605382e132-true-d1605659e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1605382e132-true-d1605659e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1605647e12-true-d1605688e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605647e12-true-d1605688e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1605647e12-true-d1605688e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1605382e143-true-d1605739e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605382e143-true-d1605739e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1605382e143-true-d1605739e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1605713e58-true-d1605800e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605713e58-true-d1605800e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1605713e58-true-d1605800e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1605254e11-true-d1605877e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605254e11-true-d1605877e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1605254e11-true-d1605877e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1605254e20-true-d1605930e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605254e20-true-d1605930e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1605254e20-true-d1605930e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1605254e24-true-d1605985e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605254e24-true-d1605985e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1605254e24-true-d1605985e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1605978e10-true-d1606012e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1605978e10-true-d1606012e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1605978e10-true-d1606012e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d1603021e238-true-d1606140e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1603021e238-true-d1606140e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d1603021e238-true-d1606140e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1606043e11-true-d1606248e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606043e11-true-d1606248e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1606043e11-true-d1606248e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1606043e22-true-d1606310e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606043e22-true-d1606310e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1606043e22-true-d1606310e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1606043e78-true-d1606388e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606043e78-true-d1606388e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1606043e78-true-d1606388e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d1606043e80-true-d1606449e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606043e80-true-d1606449e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d1606043e80-true-d1606449e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d1606043e83-true-d1606507e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606043e83-true-d1606507e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d1606043e83-true-d1606507e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[@typeCode = 'REFR'][hl7:observation[@classCode = 'OBS'][@moodCode = 'EVN']]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:statusCode[@code = 'completed']])]"
         id="d1600959e772-true-d1606550e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e772-true-d1606550e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:statusCode[@code = 'completed']] (rule-reference: d1600959e772-true-d1606550e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[@typeCode = 'REFR'][hl7:observation[@classCode = 'OBS'][@moodCode = 'EVN']]/hl7:observation[hl7:statusCode[@code = 'completed']]/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:code[@nullFlavor='OTH'] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime[not(@nullFlavor)] | self::hl7:effectiveTime[@nullFlavor='UNK'] | self::hl7:value[@xsi:type='PQ'] | self::hl7:value[@xsi:type='IVL_PQ'] | self::hl7:value[@xsi:type='INT'] | self::hl7:value[@xsi:type='IVL_INT'] | self::hl7:value[@xsi:type='BL'] | self::hl7:value[@xsi:type='ST'] | self::hl7:value[@xsi:type='CV'] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.186-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:value[(@code = '281268007' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | self::hl7:value[(@code = '255599008' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | self::hl7:value[@xsi:type='CD'] | self::hl7:value[@xsi:type='RTO'] | self::hl7:value[@xsi:type='RTO_PQ_PQ'] | self::hl7:interpretationCode[not(@nullFlavor)] | self::hl7:interpretationCode[@nullFlavor='OTH'])]"
         id="d1600959e781-true-d1606583e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e781-true-d1606583e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:code[@nullFlavor='OTH'] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime[not(@nullFlavor)] | hl7:effectiveTime[@nullFlavor='UNK'] | hl7:value[@xsi:type='PQ'] | hl7:value[@xsi:type='IVL_PQ'] | hl7:value[@xsi:type='INT'] | hl7:value[@xsi:type='IVL_INT'] | hl7:value[@xsi:type='BL'] | hl7:value[@xsi:type='ST'] | hl7:value[@xsi:type='CV'] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.186-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:value[(@code = '281268007' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | hl7:value[(@code = '255599008' and @codeSystem = '2.16.840.1.113883.6.96') or @nullFlavor] | hl7:value[@xsi:type='CD'] | hl7:value[@xsi:type='RTO'] | hl7:value[@xsi:type='RTO_PQ_PQ'] | hl7:interpretationCode[not(@nullFlavor)] | hl7:interpretationCode[@nullFlavor='OTH'] (rule-reference: d1600959e781-true-d1606583e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:entryRelationship[@typeCode = 'REFR'][hl7:observation[@classCode = 'OBS'][@moodCode = 'EVN']]/hl7:observation[hl7:statusCode[@code = 'completed']]/hl7:value[@xsi:type='PQ']/*[not(@xsi:nil = 'true')][not(self::hl7:translation)]"
         id="d1606605e23-true-d1606641e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.23-2021-01-19T145323.html"
              test="not(.)">(atcdabbr_other_LaboratoryObservationValue)/d1606605e23-true-d1606641e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation (rule-reference: d1606605e23-true-d1606641e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/*[not(@xsi:nil = 'true')][not(self::hl7:observationRange[hl7:interpretationCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.83')]])]"
         id="d1600959e894-true-d1606724e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e894-true-d1606724e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationRange[hl7:interpretationCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.83')]] (rule-reference: d1600959e894-true-d1606724e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.83')]]/*[not(@xsi:nil = 'true')][not(self::hl7:text | self::hl7:value | self::hl7:interpretationCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.83')])]"
         id="d1600959e1113-true-d1606753e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e1113-true-d1606753e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:text | hl7:value | hl7:interpretationCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.83')] (rule-reference: d1600959e1113-true-d1606753e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.83')]]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1606757e54-true-d1606769e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1606757e54-true-d1606769e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1606757e54-true-d1606769e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.27'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.6']]/hl7:referenceRange[@typeCode = 'REFV'][hl7:observationRange[@classCode = 'OBS'][@moodCode = 'EVN.CRT']]/hl7:observationRange[hl7:interpretationCode[(@code = 'N' and @codeSystem = '2.16.840.1.113883.5.83')]]/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:low[not(@nullFlavor)] | self::hl7:low[@nullFlavor='NA'] | self::hl7:low[@nullFlavor='NINF'] | self::hl7:high[not(@nullFlavor)] | self::hl7:high[@nullFlavor='NA'] | self::hl7:high[@nullFlavor='PINF'])]"
         id="d1600959e1125-true-d1606783e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1600959e1125-true-d1606783e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[not(@nullFlavor)] | hl7:low[@nullFlavor='NA'] | hl7:low[@nullFlavor='NINF'] | hl7:high[not(@nullFlavor)] | hl7:high[@nullFlavor='NA'] | hl7:high[@nullFlavor='PINF'] (rule-reference: d1600959e1125-true-d1606783e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']])]"
         id="d42e27025-true-d1607271e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d42e27025-true-d1607271e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']] (rule-reference: d42e27025-true-d1607271e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19'] | self::hl7:value[not(@nullFlavor)] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole])]"
         id="d1606812e4-true-d1607759e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606812e4-true-d1607759e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19'] | hl7:value[not(@nullFlavor)] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] (rule-reference: d1606812e4-true-d1607759e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d1606812e55-true-d1607914e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606812e55-true-d1607914e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d1606812e55-true-d1607914e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1607775e14-true-d1608020e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1607775e14-true-d1608020e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1607775e14-true-d1608020e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1608024e101-true-d1608093e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608024e101-true-d1608093e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1608024e101-true-d1608093e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1608024e174-true-d1608169e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1608024e174-true-d1608169e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1608024e174-true-d1608169e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1608024e186-true-d1608191e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1608024e186-true-d1608191e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1608024e186-true-d1608191e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1608179e12-true-d1608220e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1608179e12-true-d1608220e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1608179e12-true-d1608220e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1608024e198-true-d1608271e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608024e198-true-d1608271e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1608024e198-true-d1608271e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1608245e58-true-d1608332e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608245e58-true-d1608332e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1608245e58-true-d1608332e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1606812e57-true-d1608499e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606812e57-true-d1608499e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1606812e57-true-d1608499e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1608377e41-true-d1608607e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608377e41-true-d1608607e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1608377e41-true-d1608607e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1608377e60-true-d1608666e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608377e60-true-d1608666e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1608377e60-true-d1608666e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1608377e106-true-d1608723e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608377e106-true-d1608723e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1608377e106-true-d1608723e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1608727e92-true-d1608757e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1608727e92-true-d1608757e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1608727e92-true-d1608757e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1608377e129-true-d1608805e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608377e129-true-d1608805e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1608377e129-true-d1608805e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1608377e145-true-d1608850e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608377e145-true-d1608850e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1608377e145-true-d1608850e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1608820e67-true-d1608913e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608820e67-true-d1608913e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1608820e67-true-d1608913e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1606812e59-true-d1609081e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606812e59-true-d1609081e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1606812e59-true-d1609081e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1608958e5-true-d1609213e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608958e5-true-d1609213e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1608958e5-true-d1609213e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1609086e50-true-d1609279e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609086e50-true-d1609279e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1609086e50-true-d1609279e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1609086e120-true-d1609341e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609086e120-true-d1609341e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1609086e120-true-d1609341e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1609086e132-true-d1609363e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609086e132-true-d1609363e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1609086e132-true-d1609363e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1609351e12-true-d1609392e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609351e12-true-d1609392e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1609351e12-true-d1609392e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1609086e143-true-d1609443e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609086e143-true-d1609443e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1609086e143-true-d1609443e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1609417e58-true-d1609504e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609417e58-true-d1609504e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1609417e58-true-d1609504e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1608958e7-true-d1609581e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608958e7-true-d1609581e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1608958e7-true-d1609581e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1608958e16-true-d1609634e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608958e16-true-d1609634e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1608958e16-true-d1609634e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1608958e20-true-d1609689e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1608958e20-true-d1609689e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1608958e20-true-d1609689e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1609682e10-true-d1609716e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609682e10-true-d1609716e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1609682e10-true-d1609716e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d1606812e61-true-d1609844e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1606812e61-true-d1609844e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d1606812e61-true-d1609844e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1609747e11-true-d1609952e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609747e11-true-d1609952e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1609747e11-true-d1609952e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1609747e22-true-d1610014e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609747e22-true-d1610014e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1609747e22-true-d1610014e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1609747e78-true-d1610092e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609747e78-true-d1610092e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1609747e78-true-d1610092e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d1609747e80-true-d1610153e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609747e80-true-d1610153e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d1609747e80-true-d1610153e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]/hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d1609747e83-true-d1610211e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1609747e83-true-d1610211e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d1609747e83-true-d1610211e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']])]"
         id="d42e27032-true-d1610705e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d42e27032-true-d1610705e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']] (rule-reference: d42e27032-true-d1610705e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole])]"
         id="d1610234e158-true-d1611225e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1610234e158-true-d1611225e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '48767-8' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] (rule-reference: d1610234e158-true-d1611225e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1611256e54-true-d1611268e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1611256e54-true-d1611268e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1611256e54-true-d1611268e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d1610234e228-true-d1611423e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1610234e228-true-d1611423e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d1610234e228-true-d1611423e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1611284e14-true-d1611529e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611284e14-true-d1611529e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1611284e14-true-d1611529e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1611533e101-true-d1611602e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611533e101-true-d1611602e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1611533e101-true-d1611602e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1611533e174-true-d1611678e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1611533e174-true-d1611678e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1611533e174-true-d1611678e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1611533e186-true-d1611700e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1611533e186-true-d1611700e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1611533e186-true-d1611700e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1611688e12-true-d1611729e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d1611688e12-true-d1611729e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1611688e12-true-d1611729e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1611533e198-true-d1611780e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611533e198-true-d1611780e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1611533e198-true-d1611780e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1611754e58-true-d1611841e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611754e58-true-d1611841e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1611754e58-true-d1611841e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1610234e231-true-d1612008e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1610234e231-true-d1612008e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1610234e231-true-d1612008e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1611886e45-true-d1612116e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611886e45-true-d1612116e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1611886e45-true-d1612116e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1611886e64-true-d1612175e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611886e64-true-d1612175e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1611886e64-true-d1612175e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1611886e110-true-d1612232e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611886e110-true-d1612232e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1611886e110-true-d1612232e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1612236e92-true-d1612266e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1612236e92-true-d1612266e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1612236e92-true-d1612266e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1611886e133-true-d1612314e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611886e133-true-d1612314e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1611886e133-true-d1612314e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1611886e149-true-d1612359e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1611886e149-true-d1612359e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1611886e149-true-d1612359e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1612329e67-true-d1612422e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612329e67-true-d1612422e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1612329e67-true-d1612422e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1610234e237-true-d1612590e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1610234e237-true-d1612590e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1610234e237-true-d1612590e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1612467e9-true-d1612722e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612467e9-true-d1612722e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1612467e9-true-d1612722e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1612595e50-true-d1612788e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612595e50-true-d1612788e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1612595e50-true-d1612788e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1612595e120-true-d1612850e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612595e120-true-d1612850e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1612595e120-true-d1612850e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1612595e132-true-d1612872e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612595e132-true-d1612872e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1612595e132-true-d1612872e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1612860e12-true-d1612901e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612860e12-true-d1612901e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1612860e12-true-d1612901e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1612595e143-true-d1612952e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612595e143-true-d1612952e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1612595e143-true-d1612952e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1612926e58-true-d1613013e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612926e58-true-d1613013e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1612926e58-true-d1613013e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1612467e11-true-d1613090e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612467e11-true-d1613090e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1612467e11-true-d1613090e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1612467e20-true-d1613143e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612467e20-true-d1613143e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1612467e20-true-d1613143e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1612467e24-true-d1613198e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1612467e24-true-d1613198e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1612467e24-true-d1613198e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1613191e10-true-d1613225e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1613191e10-true-d1613225e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1613191e10-true-d1613225e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d1610234e243-true-d1613353e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1610234e243-true-d1613353e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d1610234e243-true-d1613353e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d1613256e11-true-d1613461e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1613256e11-true-d1613461e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d1613256e11-true-d1613461e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1613256e22-true-d1613523e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1613256e22-true-d1613523e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1613256e22-true-d1613523e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d1613256e78-true-d1613601e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1613256e78-true-d1613601e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d1613256e78-true-d1613601e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d1613256e80-true-d1613662e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1613256e80-true-d1613662e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d1613256e80-true-d1613662e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.26'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.4']]/hl7:component[hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]]/hl7:act[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.11'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.40'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.2']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:desc)]"
         id="d1613256e83-true-d1613720e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.26-2019-05-29T105134.html"
              test="not(.)">(atcdabbr_entry_LaboratoryBatteryOrganizer)/d1613256e83-true-d1613720e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:desc (rule-reference: d1613256e83-true-d1613720e0)</assert>
   </rule>
</pattern>
