<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.170
Name: Case Identification
Description:  Die Case Identification kann verwendet werden, um die EMS Fall-ID anzugeben, die im Rahmen einer EMS-Meldung vergeben wurde. Sollte eine Meldung an das EMS fehlgeschlagen sein, kann dieser Umstand mit observation/id[@nullFlavor='NI'] codiert werden. 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713-closed">
   <title>Case Identification</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']])]"
         id="d42e24564-true-d1383816e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(.)">(atcdabbr_entry_caseIdentification)/d42e24564-true-d1383816e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']] (rule-reference: d42e24564-true-d1383816e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] | self::hl7:id[@root = '1.2.40.0.34.3.1.1'] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id | self::hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d42e24599-true-d1383885e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(.)">(atcdabbr_entry_caseIdentification)/d42e24599-true-d1383885e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] | hl7:id[@root = '1.2.40.0.34.3.1.1'] | hl7:id[@nullFlavor='NI'] | hl7:id | hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d42e24599-true-d1383885e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1383921e54-true-d1383933e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1383921e54-true-d1383933e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1383921e54-true-d1383933e0)</assert>
   </rule>
</pattern>
