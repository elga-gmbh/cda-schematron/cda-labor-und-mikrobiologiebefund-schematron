<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.169
Name: Angeforderte Untersuchung Entry
Description: Entry zur codierten Darstellung einer angeforderten Untersuchung. 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708">
   <title>Angeforderte Untersuchung Entry</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.169
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]
Item: (atlab_entry_AngeforderteUntersuchungEntry)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.169
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]
Item: (atlab_entry_AngeforderteUntersuchungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]"
         id="d42e24488-false-d1383182e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="string(@classCode) = ('PROC')">(atlab_entry_AngeforderteUntersuchungEntry): Der Wert von classCode MUSS 'PROC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="string(@moodCode) = ('RQO')">(atlab_entry_AngeforderteUntersuchungEntry): Der Wert von moodCode MUSS 'RQO' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']) &gt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']) &lt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="count(hl7:id) &lt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Element hl7:id kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:code[not(@nullFlavor)] | hl7:code[@nullFlavor='OTH'])"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="$elmcount &gt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Auswahl (hl7:code[not(@nullFlavor)]  oder  hl7:code[@nullFlavor='OTH']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="$elmcount &lt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Auswahl (hl7:code[not(@nullFlavor)]  oder  hl7:code[@nullFlavor='OTH']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="count(hl7:code[not(@nullFlavor)]) &lt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Element hl7:code[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="count(hl7:code[@nullFlavor='OTH']) &lt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Element hl7:code[@nullFlavor='OTH'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="count(hl7:text) &lt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Element hl7:text kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="count(hl7:effectiveTime) &lt;= 1">(atlab_entry_AngeforderteUntersuchungEntry): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.169
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']
Item: (atlab_entry_AngeforderteUntersuchungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']"
         id="d42e24494-false-d1383248e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_entry_AngeforderteUntersuchungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.169')">(atlab_entry_AngeforderteUntersuchungEntry): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.169' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.169
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:id
Item: (atlab_entry_AngeforderteUntersuchungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:id"
         id="d42e24502-false-d1383262e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_entry_AngeforderteUntersuchungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.169
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:code[not(@nullFlavor)]
Item: (atlab_entry_AngeforderteUntersuchungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:code[not(@nullFlavor)]"
         id="d42e24513-false-d1383270e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atlab_entry_AngeforderteUntersuchungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="@code">(atlab_entry_AngeforderteUntersuchungEntry): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atlab_entry_AngeforderteUntersuchungEntry): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="@codeSystem">(atlab_entry_AngeforderteUntersuchungEntry): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(atlab_entry_AngeforderteUntersuchungEntry): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_entry_AngeforderteUntersuchungEntry): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="@displayName">(atlab_entry_AngeforderteUntersuchungEntry): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atlab_entry_AngeforderteUntersuchungEntry): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.169
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:code[@nullFlavor='OTH']
Item: (atlab_entry_AngeforderteUntersuchungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:code[@nullFlavor='OTH']"
         id="d42e24533-false-d1383300e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atlab_entry_AngeforderteUntersuchungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="hl7:translation[not(@nullFlavor)]">(atlab_entry_AngeforderteUntersuchungEntry): Wenn code[@nullFlavor='OTH'] dann MUSS "code/translation" anwesend sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:text
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:text"
         id="d1383301e67-false-d1383311e0">
      <extends rule="ED"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d1383301e69-false-d1383330e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="@value">(atcdabrr_other_NarrativeTextReference): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="starts-with(@value,'#') or starts-with(@value,'http')">(atcdabrr_other_NarrativeTextReference): The @value attribute content MUST conform to the format '#xxx', where xxx is the ID of the corresponding 'content'-element, or begin with the 'http' or 'https' url-scheme.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.169
Context: *[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:effectiveTime
Item: (atlab_entry_AngeforderteUntersuchungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]]/hl7:procedure[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.169']]/hl7:effectiveTime"
         id="d42e24556-false-d1383344e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(atlab_entry_AngeforderteUntersuchungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
