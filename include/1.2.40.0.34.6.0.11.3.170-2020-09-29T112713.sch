<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.170
Name: Case Identification
Description:  Die Case Identification kann verwendet werden, um die EMS Fall-ID anzugeben, die im Rahmen einer EMS-Meldung vergeben wurde. Sollte eine Meldung an das EMS fehlgeschlagen sein, kann dieser Umstand mit observation/id[@nullFlavor='NI'] codiert werden. 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713">
   <title>Case Identification</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]
Item: (atcdabbr_entry_caseIdentification)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]"
         id="d42e24599-false-d1383492e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="string(@classCode) = ('CASE')">(atcdabbr_entry_caseIdentification): Der Wert von classCode MUSS 'CASE' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="string(@moodCode) = ('EVN')">(atcdabbr_entry_caseIdentification): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170']) &gt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170']) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']) &gt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:id[@root = '1.2.40.0.34.3.1.1'] | hl7:id[@nullFlavor='NI'])"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="$elmcount &lt;= 1">(atcdabbr_entry_caseIdentification): Auswahl (hl7:id[@root = '1.2.40.0.34.3.1.1']  oder  hl7:id[@nullFlavor='NI']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:id[@root = '1.2.40.0.34.3.1.1']) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:id[@root = '1.2.40.0.34.3.1.1'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:id[@nullFlavor='NI']) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:id[@nullFlavor='NI'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')]) &gt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')]) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:text) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:text kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:effectiveTime) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(atcdabbr_entry_caseIdentification): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170']
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170']"
         id="d42e24605-false-d1383598e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.170')">(atcdabbr_entry_caseIdentification): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.170' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']"
         id="d42e24613-false-d1383613e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.1.1.2')">(atcdabbr_entry_caseIdentification): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.1.1.2' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:id[@root = '1.2.40.0.34.3.1.1']
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:id[@root = '1.2.40.0.34.3.1.1']"
         id="d42e24623-false-d1383628e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="string(@root) = ('1.2.40.0.34.3.1.1')">(atcdabbr_entry_caseIdentification): Der Wert von root MUSS '1.2.40.0.34.3.1.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@extension">(atcdabbr_entry_caseIdentification): Attribut @extension MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_caseIdentification): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:id[@nullFlavor='NI']
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:id[@nullFlavor='NI']"
         id="d42e24637-false-d1383646e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:id
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:id"
         id="d42e24645-false-d1383656e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')]
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:code[(@code = '416341003' and @codeSystem = '2.16.840.1.113883.6.96')]"
         id="d42e24654-false-d1383667e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@nullFlavor or (@code='416341003' and @codeSystem='2.16.840.1.113883.6.96' and @displayName='Case Management Started')">(atcdabbr_entry_caseIdentification): Der Elementinhalt MUSS einer von 'code '416341003' codeSystem '2.16.840.1.113883.6.96' displayName='Case Management Started'' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="string(@codeSystemName) = ('SNOMED CT') or not(@codeSystemName)">(atcdabbr_entry_caseIdentification): Der Wert von codeSystemName MUSS 'SNOMED CT' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_caseIdentification): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:text
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:text"
         id="d1383668e75-false-d1383690e0">
      <extends rule="ED"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d1383668e77-false-d1383709e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="@value">(atcdabrr_other_NarrativeTextReference): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="starts-with(@value,'#') or starts-with(@value,'http')">(atcdabrr_other_NarrativeTextReference): The @value attribute content MUST conform to the format '#xxx', where xxx is the ID of the corresponding 'content'-element, or begin with the 'http' or 'https' url-scheme.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:statusCode[@code = 'completed']
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:statusCode[@code = 'completed']"
         id="d42e24667-false-d1383724e0">
      <extends rule="CS"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@nullFlavor or (@code='completed')">(atcdabbr_entry_caseIdentification): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:effectiveTime
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:effectiveTime"
         id="d42e24672-false-d1383740e0">
      <extends rule="IVL_TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_TS')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.170
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (atcdabbr_entry_caseIdentification)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e24678-false-d1383753e0">
      <extends rule="CD"/>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.6.0.10.19-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(atcdabbr_entry_caseIdentification): Der Elementinhalt MUSS einer von '1.2.40.0.34.6.0.10.19 EMS Meldepflichtige Krankheiten VS (DYNAMIC)' sein.</assert>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_caseIdentification): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@code">(atcdabbr_entry_caseIdentification): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_caseIdentification): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@codeSystem">(atcdabbr_entry_caseIdentification): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$')">(atcdabbr_entry_caseIdentification): Attribute @codeSystem MUSS vom Datentyp 'oid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_caseIdentification): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="@displayName">(atcdabbr_entry_caseIdentification): Attribut @displayName MUSS vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_caseIdentification): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
</pattern>
